﻿using Identity.Common.DataTypes;
using System.Web.Services;

namespace Identity.WebService
{
    /// <summary>
    /// Summary description for UserService
    /// </summary>
    [WebService(Namespace = "http://Identity.WebService.Danske.com", Description = "Provides data related to a users in Active Directory")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]

    public class UserService : System.Web.Services.WebService
    {
        private ADConnector.ADConncetor addConnector = null;

        public UserService()
        {
            addConnector = new ADConnector.ADConncetor();
        }
        [WebMethod]
        public UserInfo GetUserADDataByUserId(string userId)
        {
            return addConnector.GetUserADDataByUserId(userId);
        }

    }
}
